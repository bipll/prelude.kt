package com.gitlab.bipll.prelude.test

import com.gitlab.bipll.prelude.*
import kotlin.test

@Test
fun typesAreSame() {
	assertTrue(isAnyOf1<Int, Int>())
	assertFalse(isAnyOf1<Int, Double>())
	assertFalse(isAnyOf1<Int, Any?>())

	assertTrue(isAnyOf2<Int, Int, Double>())
	assertTrue(isAnyOf2<Int, Double, Int>())
	assertFalse(isAnyOf2<Int, Double, Any?>())

	assertTrue(isAnyOf3<Int, Int, Double, Any?>())
	assertTrue(isAnyOf3<Int, Double, Int, Any?>())
	assertTrue(isAnyOf3<Int, Double, Any?, Int>())
	assertFalse(isAnyOf3<Int, Double, Any?, Double>())

	assertTrue(isAnyOf4<Int, Int, Double, Any?, Double>())
	assertTrue(isAnyOf4<Int, Double, Int, Any?, Double>())
	assertTrue(isAnyOf4<Int, Double, Any?, Int, Double>())
	assertTrue(isAnyOf4<Int, Double, Any?, Double, Int>())
	assertFalse(isAnyOf4<Int, Double, Any?, Double, Any?>())

	assertTrue(isAnyOf5<Int, Int, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf5<Int, Any?, Int, Double, Any?, Double>())
	assertTrue(isAnyOf5<Int, Any?, Double, Int, Any?, Double>())
	assertTrue(isAnyOf5<Int, Any?, Double, Any?, Int, Double>())
	assertTrue(isAnyOf5<Int, Any?, Double, Any?, Double, Int>())
	assertFalse(isAnyOf5<Int, Double, Any?, Double, Any?, Double>())

	assertTrue(isAnyOf6<Int, Int, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf6<Int, Any?, Int, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf6<Int, Any?, Double, Int, Any?, Double, Any?>())
	assertTrue(isAnyOf6<Int, Any?, Double, Any?, Int, Double, Any?>())
	assertTrue(isAnyOf6<Int, Any?, Double, Any?, Double, Int, Any?>())
	assertTrue(isAnyOf6<Int, Any?, Double, Any?, Double, Any?, Int>())
	assertFalse(isAnyOf6<Int, Double, Any?, Double, Any?, Double, Any?>())

	assertTrue(isAnyOf7<Int, Int, Double, Any?, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf7<Int, Any?, Int, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf7<Int, Any?, Double, Int, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf7<Int, Any?, Double, Any?, Int, Double, Any?, Double>())
	assertTrue(isAnyOf7<Int, Any?, Double, Any?, Double, Int, Any?, Double>())
	assertTrue(isAnyOf7<Int, Any?, Double, Any?, Double, Any?, Int, Double>())
	assertTrue(isAnyOf7<Int, Any?, Double, Any?, Double, Any?, Double, Int>())
	assertFalse(isAnyOf7<Int, Double, Any?, Double, Any?, Double, Any?, Double>())

	assertTrue(isAnyOf8<Int, Int, Double, Any?, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf8<Int, Any?, Int, Double, Any?, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Int, Any?, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Any?, Int, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Any?, Double, Int, Any?, Double, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Any?, Double, Any?, Int, Double, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Any?, Double, Any?, Double, Int, Any?>())
	assertTrue(isAnyOf8<Int, Any?, Double, Any?, Double, Any?, Double, Any?, Int>())
	assertFalse(isAnyOf8<Int, Double, Any?, Double, Any?, Double, Any?, Double, Any?>())

	assertTrue(isAnyOf9<Int, Int, Double, Any?, Double, Any?, Double, Any?, Double, Any?>())
	assertTrue(isAnyOf9<Int, Any?, Int, Double, Any?, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Int, Any?, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Int, Double, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Double, Int, Any?, Double, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Double, Any?, Int, Double, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Double, Any?, Double, Int, Any?, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Double, Any?, Double, Any?, Int, Double>())
	assertTrue(isAnyOf9<Int, Any?, Double, Any?, Double, Any?, Double, Any?, Double, Int>())
	assertFalse(isAnyOf9<Int, Double, Any?, Double, Any?, Double, Any?, Double, Any?, Double>())

	assertTrue(isSame<Int, Int>())
	assertFalse(isSame<Int, Double>())
	assertFalse(isSame<Int, Any?>())
}
