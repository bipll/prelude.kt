package com.gitlab.bipll.prelude.test

import com.gitlab.bipll.prelude.*
import kotlin.test

@Test
fun boolByOptional() {
    val nl: Int? = null
    val nnl: Int? = 42

    assertNull(false * nl)
    assertNull(false * nnl)
    assertNull(true * nl)
    assertEquals(true * nnl, nnl)

    assertNull(guard(false, nl))
    assertNull(guard(false, nnl))
    assertNull(guard(true, nl))
    assertEquals(guard(true, nnl), nnl)

    assertNull(guard(nl, nl))
    assertNull(guard(nl, nnl))
    assertNull(guard(nnl, nl))
    assertEquals(guard(nnl, nnl), nnl)
}
