package com.gitlab.bipll.prelude


inline operator fun<reified R, reified Q> Callable0<R>.times(
	crossinline g: Callable0<Q>
): Callable0<R> = {
	g(); this()
}

inline operator fun<reified R, reified Q, reified A1> Callable0<R>.times(
	crossinline g: Callable1<Q, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2> Callable0<R>.times(
	crossinline g: Callable2<Q, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3> Callable0<R>.times(
	crossinline g: Callable3<Q, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4> Callable0<R>.times(
	crossinline g: Callable4<Q, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4, reified A5> Callable0<R>.times(
	crossinline g: Callable5<Q, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable0<R>.times(
	crossinline g: Callable6<Q, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable0<R>.times(
	crossinline g: Callable7<Q, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable0<R>.times(
	crossinline g: Callable8<Q, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8); this()
}

inline operator fun<reified R, reified Q, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable0<R>.times(
	crossinline g: Callable9<Q, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9); this()
}



@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1> Callable1<R, B1>.times(
	crossinline g: Callable0<B1>
): Callable0<R> = {
	this(g())
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1> Callable1<R, B1>.times(
	crossinline g: Callable1<B1, A1>
): Callable1<R, A1> = {
	a1: A1 -> this(g(a1))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2> Callable1<R, B1>.times(
	crossinline g: Callable2<B1, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> this(g(a1, a2))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3> Callable1<R, B1>.times(
	crossinline g: Callable3<B1, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> this(g(a1, a2, a3))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4> Callable1<R, B1>.times(
	crossinline g: Callable4<B1, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> this(g(a1, a2, a3, a4))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5> Callable1<R, B1>.times(
	crossinline g: Callable5<B1, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> this(g(a1, a2, a3, a4, a5))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable1<R, B1>.times(
	crossinline g: Callable6<B1, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> this(g(a1, a2, a3, a4, a5, a6))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable1<R, B1>.times(
	crossinline g: Callable7<B1, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> this(g(a1, a2, a3, a4, a5, a6, a7))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable1<R, B1>.times(
	crossinline g: Callable8<B1, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> this(g(a1, a2, a3, a4, a5, a6, a7, a8))
}

@JvmName("unliftedTimes")
inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable1<R, B1>.times(
	crossinline g: Callable9<B1, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> this(g(a1, a2, a3, a4, a5, a6, a7, a8, a9))
}



inline operator fun<reified R, reified B1> Callable1<R, B1>.times(
	crossinline g: Callable0<out Tuple1<B1>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified A1> Callable1<R, B1>.times(
	crossinline g: Callable1<out Tuple1<B1>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2> Callable1<R, B1>.times(
	crossinline g: Callable2<out Tuple1<B1>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3> Callable1<R, B1>.times(
	crossinline g: Callable3<out Tuple1<B1>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4> Callable1<R, B1>.times(
	crossinline g: Callable4<out Tuple1<B1>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5> Callable1<R, B1>.times(
	crossinline g: Callable5<out Tuple1<B1>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable1<R, B1>.times(
	crossinline g: Callable6<out Tuple1<B1>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable1<R, B1>.times(
	crossinline g: Callable7<out Tuple1<B1>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable1<R, B1>.times(
	crossinline g: Callable8<out Tuple1<B1>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable1<R, B1>.times(
	crossinline g: Callable9<out Tuple1<B1>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2> Callable2<R, B1, B2>.times(
	crossinline g: Callable0<out Tuple2<B1, B2>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1> Callable2<R, B1, B2>.times(
	crossinline g: Callable1<out Tuple2<B1, B2>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2> Callable2<R, B1, B2>.times(
	crossinline g: Callable2<out Tuple2<B1, B2>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3> Callable2<R, B1, B2>.times(
	crossinline g: Callable3<out Tuple2<B1, B2>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4> Callable2<R, B1, B2>.times(
	crossinline g: Callable4<out Tuple2<B1, B2>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4, reified A5> Callable2<R, B1, B2>.times(
	crossinline g: Callable5<out Tuple2<B1, B2>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable2<R, B1, B2>.times(
	crossinline g: Callable6<out Tuple2<B1, B2>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable2<R, B1, B2>.times(
	crossinline g: Callable7<out Tuple2<B1, B2>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable2<R, B1, B2>.times(
	crossinline g: Callable8<out Tuple2<B1, B2>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable2<R, B1, B2>.times(
	crossinline g: Callable9<out Tuple2<B1, B2>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable0<out Tuple3<B1, B2, B3>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable1<out Tuple3<B1, B2, B3>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable2<out Tuple3<B1, B2, B3>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable3<out Tuple3<B1, B2, B3>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable4<out Tuple3<B1, B2, B3>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable5<out Tuple3<B1, B2, B3>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable6<out Tuple3<B1, B2, B3>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable7<out Tuple3<B1, B2, B3>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable8<out Tuple3<B1, B2, B3>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable3<R, B1, B2, B3>.times(
	crossinline g: Callable9<out Tuple3<B1, B2, B3>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable0<out Tuple4<B1, B2, B3, B4>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable1<out Tuple4<B1, B2, B3, B4>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable2<out Tuple4<B1, B2, B3, B4>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable3<out Tuple4<B1, B2, B3, B4>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable4<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable5<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable6<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable7<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable8<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable4<R, B1, B2, B3, B4>.times(
	crossinline g: Callable9<out Tuple4<B1, B2, B3, B4>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable0<out Tuple5<B1, B2, B3, B4, B5>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable1<out Tuple5<B1, B2, B3, B4, B5>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable2<out Tuple5<B1, B2, B3, B4, B5>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable3<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable4<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable5<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable6<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable7<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable8<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable5<R, B1, B2, B3, B4, B5>.times(
	crossinline g: Callable9<out Tuple5<B1, B2, B3, B4, B5>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable0<out Tuple6<B1, B2, B3, B4, B5, B6>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable1<out Tuple6<B1, B2, B3, B4, B5, B6>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable2<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable3<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable4<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable5<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable6<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable7<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable8<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable6<R, B1, B2, B3, B4, B5, B6>.times(
	crossinline g: Callable9<out Tuple6<B1, B2, B3, B4, B5, B6>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable0<out Tuple7<B1, B2, B3, B4, B5, B6, B7>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable1<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable2<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable3<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable4<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable5<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable6<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable7<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable8<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable7<R, B1, B2, B3, B4, B5, B6, B7>.times(
	crossinline g: Callable9<out Tuple7<B1, B2, B3, B4, B5, B6, B7>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable0<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable1<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable2<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable3<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable4<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable5<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable6<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable7<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable8<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable8<R, B1, B2, B3, B4, B5, B6, B7, B8>.times(
	crossinline g: Callable9<out Tuple8<B1, B2, B3, B4, B5, B6, B7, B8>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}



inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable0<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>>
): Callable0<R> = {
	g() feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable1<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1>
): Callable1<R, A1> = {
	a1: A1 -> g(a1) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable2<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2>
): Callable2<R, A1, A2> = {
	a1: A1, a2: A2 -> g(a1, a2) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable3<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3>
): Callable3<R, A1, A2, A3> = {
	a1: A1, a2: A2, a3: A3 -> g(a1, a2, a3) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable4<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4>
): Callable4<R, A1, A2, A3, A4> = {
	a1: A1, a2: A2, a3: A3, a4: A4 -> g(a1, a2, a3, a4) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4, reified A5> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable5<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4, A5>
): Callable5<R, A1, A2, A3, A4, A5> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5 -> g(a1, a2, a3, a4, a5) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable6<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4, A5, A6>
): Callable6<R, A1, A2, A3, A4, A5, A6> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6 -> g(a1, a2, a3, a4, a5, a6) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable7<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4, A5, A6, A7>
): Callable7<R, A1, A2, A3, A4, A5, A6, A7> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7 -> g(a1, a2, a3, a4, a5, a6, a7) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable8<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4, A5, A6, A7, A8>
): Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8 -> g(a1, a2, a3, a4, a5, a6, a7, a8) feed this
}

inline operator fun<reified R, reified B1, reified B2, reified B3, reified B4, reified B5, reified B6, reified B7, reified B8, reified B9,
		    reified A1, reified A2, reified A3, reified A4, reified A5, reified A6, reified A7, reified A8, reified A9> Callable9<R, B1, B2, B3, B4, B5, B6, B7, B8, B9>.times(
	crossinline g: Callable9<out Tuple9<B1, B2, B3, B4, B5, B6, B7, B8, B9>, A1, A2, A3, A4, A5, A6, A7, A8, A9>
): Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = {
	a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9 -> g(a1, a2, a3, a4, a5, a6, a7, a8, a9) feed this
}
