package com.gitlab.bipll.prelude

typealias Callable0<R> = () -> R
typealias Callable1<R, A1> = (A1) -> R
typealias Callable2<R, A1, A2> = (A1, A2) -> R
typealias Callable3<R, A1, A2, A3> = (A1, A2, A3) -> R
typealias Callable4<R, A1, A2, A3, A4> = (A1, A2, A3, A4) -> R
typealias Callable5<R, A1, A2, A3, A4, A5> = (A1, A2, A3, A4, A5) -> R
typealias Callable6<R, A1, A2, A3, A4, A5, A6> = (A1, A2, A3, A4, A5, A6) -> R
typealias Callable7<R, A1, A2, A3, A4, A5, A6, A7> = (A1, A2, A3, A4, A5, A6, A7) -> R
typealias Callable8<R, A1, A2, A3, A4, A5, A6, A7, A8> = (A1, A2, A3, A4, A5, A6, A7, A8) -> R
typealias Callable9<R, A1, A2, A3, A4, A5, A6, A7, A8, A9> = (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R


inline fun<reified T, reified T1> isAnyOf1(): Boolean = T::class == T1::class
inline fun<reified T, reified T1, reified T2> isAnyOf2(): Boolean = T::class == T1::class || T::class == T2::class
inline fun<reified T, reified T1, reified T2, reified T3> isAnyOf3(): Boolean = arrayOf(T1::class, T2::class, T3::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4> isAnyOf4(): Boolean = arrayOf(T1::class, T2::class, T3::class, T4::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4, reified T5> isAnyOf5(): Boolean =
    arrayOf(T1::class, T2::class, T3::class, T4::class, T5::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4, reified T5, reified T6> isAnyOf6(): Boolean =
	arrayOf(T1::class, T2::class, T3::class, T4::class, T5::class, T6::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4, reified T5, reified T6, reified T7> isAnyOf7(): Boolean =
	arrayOf(T1::class, T2::class, T3::class, T4::class, T5::class, T6::class, T7::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4, reified T5, reified T6, reified T7, reified T8> isAnyOf8(): Boolean =
	arrayOf(T1::class, T2::class, T3::class, T4::class, T5::class, T6::class, T7::class, T8::class).any({ it == T::class })
inline fun<reified T, reified T1, reified T2, reified T3, reified T4, reified T5, reified T6, reified T7, reified T8, reified T9> isAnyOf9(): Boolean =
	arrayOf(T1::class, T2::class, T3::class, T4::class, T5::class, T6::class, T7::class, T8::class, T9::class).any({ it == T::class })

inline fun<reified T, reified U> isSame(): Boolean = isAnyOf1<T, U>()


inline fun<reified T1, reified T2> isA(): Boolean {
	return T1::class == T2::class || T1::class.supertypes.any { it.classifier == T2::class }
}


fun toString(v: Any?) = when(v) {
	is String -> quoteString(v)
	is Char -> quoteChar(v)
	else -> v.toString()
}

private val quotedInString = charArrayOf('"', '\\')

fun quoteString(s: String): String {
	var i = s.indexOfAny(quotedInString)
	if(i < 0) return "\"$s\""
	var rv = "\"${s.substring(0, i)}\\${s[i]}"
	++i
	while(i < s.length) {
		val j = s.indexOfAny(quotedInString, i)
		if(j < 0) return "$rv${s.substring(i)}\""
		rv += s.subSequence(IntRange(i, j - 1))
		rv += '\\'
		rv += s[j]
		i = j + 1
	}
	return "$rv\""
}

fun quoteChar(c: Char) = when(c) {
	'\'' -> "'\\''"
	'\\' -> "'\\\\'"
	else -> "'$c'"
}
