package com.gitlab.bipll.prelude

internal inline operator fun<reified T : Any> Boolean.times(value: T?): T? = if(this) value else null

internal inline operator fun<reified T : Any> Boolean.times(value: ()->T?): T? = if(this) value() else null

internal inline fun<reified T : Any> guard(condition: Boolean, value: T?) = condition * value

internal inline fun<reified T : Any> guard(condition: Boolean, value: ()->T?) = condition * value

internal inline fun<reified Condition : Any, reified T : Any> guard(condition: Condition?, value: T?) = (condition != null) * value

internal inline fun<reified Condition : Any, reified T : Any> guard(condition: Condition?, value: ()->T?) = (condition != null) * value
