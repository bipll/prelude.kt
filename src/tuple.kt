package com.gitlab.bipll.prelude

open class Tuple0 {
	open val size = 0

	inline infix fun<R> feed(f: Callable0<R>) = f()

	override fun toString(): String = "Tuple()"
}

fun tuple() = Tuple0()



abstract class Tuple1<out T1>(open val t1: T1): Tuple0() {
	override val size = 1

	inline infix fun<R> feed(f: Callable1<R, in T1>): R = f(t1)

	override fun toString(): String = "Tuple(${toString(t1)})"
}


data class ValueTuple1<out T1>(override val t1: T1): Tuple1<T1>(t1) {
	override fun toString(): String = super.toString()
}

fun<T1> tuple(t1: T1) = ValueTuple1(t1)


data class Layer1_0<out R1>(override val t1: () -> R1):
Tuple1<() -> R1>(t1),
() -> Tuple1<R1> {
	constructor(tuple: Tuple1<() -> R1>): this(tuple.t1)

	override operator fun invoke() = tuple<R1>(t1())

	override fun toString(): String = super.toString()
}

fun<R1> tuple(f1: () -> R1) = Layer1_0(f1)


data class Layer1_1<out R1, in A1>(override val t1: (A1) -> R1):
Tuple1<(A1) -> R1>(t1),
(A1) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1) = tuple(t1(a1))

	override fun toString(): String = super.toString()
}

fun<R1, A1> tuple(f1: (A1) -> R1) = Layer1_1(f1)


data class Layer1_2<out R1, in A1, in A2>(override val t1: (A1, A2) -> R1):
Tuple1<(A1, A2) -> R1>(t1),
(A1, A2) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2) = tuple(t1(a1, a2))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2> tuple(f1: (A1, A2) -> R1) = Layer1_2(f1)


data class Layer1_3<out R1, in A1, in A2, in A3>(override val t1: (A1, A2, A3) -> R1):
Tuple1<(A1, A2, A3) -> R1>(t1),
(A1, A2, A3) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(t1(a1, a2, a3))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3> tuple(f1: (A1, A2, A3) -> R1) = Layer1_3(f1)


data class Layer1_4<out R1, in A1, in A2, in A3, in A4>(override val t1: (A1, A2, A3, A4) -> R1):
Tuple1<(A1, A2, A3, A4) -> R1>(t1),
(A1, A2, A3, A4) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(t1(a1, a2, a3, a4))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4> tuple(f1: (A1, A2, A3, A4) -> R1) = Layer1_4(f1)


data class Layer1_5<out R1, in A1, in A2, in A3, in A4, in A5>(override val t1: (A1, A2, A3, A4, A5) -> R1):
Tuple1<(A1, A2, A3, A4, A5) -> R1>(t1),
(A1, A2, A3, A4, A5) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4, A5) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(t1(a1, a2, a3, a4, a5))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4, A5> tuple(f1: (A1, A2, A3, A4, A5) -> R1) = Layer1_5(f1)


data class Layer1_6<out R1, in A1, in A2, in A3, in A4, in A5, in A6>(override val t1: (A1, A2, A3, A4, A5, A6) -> R1):
Tuple1<(A1, A2, A3, A4, A5, A6) -> R1>(t1),
(A1, A2, A3, A4, A5, A6) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4, A5, A6) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(t1(a1, a2, a3, a4, a5, a6))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4, A5, A6> tuple(f1: (A1, A2, A3, A4, A5, A6) -> R1) = Layer1_6(f1)


data class Layer1_7<out R1, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1):
Tuple1<(A1, A2, A3, A4, A5, A6, A7) -> R1>(t1),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4, A5, A6, A7) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(t1(a1, a2, a3, a4, a5, a6, a7))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4, A5, A6, A7> tuple(f1: (A1, A2, A3, A4, A5, A6, A7) -> R1) = Layer1_7(f1)


data class Layer1_8<out R1, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1):
Tuple1<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1>(t1),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(t1(a1, a2, a3, a4, a5, a6, a7, a8))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4, A5, A6, A7, A8> tuple(f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1) = Layer1_8(f1)


data class Layer1_9<out R1, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1):
Tuple1<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1>(t1),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple1<R1> {
	constructor(tuple: Tuple1<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1>): this(tuple.t1)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(t1(a1, a2, a3, a4, a5, a6, a7, a8, a9))

	override fun toString(): String = super.toString()
}

fun<R1, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1
) = Layer1_9<R1, A1, A2, A3, A4, A5, A6, A7, A8, A9>(f1)



abstract class Tuple2<out T1, out T2>(
	t1: T1,
	open val t2: T2
): Tuple1<T1>(t1) {
	override val size = 2

	inline infix fun<R> feed(f: Callable2<R, in T1, in T2>): R = f(t1, t2)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)})"
}


data class ValueTuple2<out T1, out T2>(
	override val t1: T1,
	override val t2: T2
): Tuple2<T1, T2>(t1, t2) {
	override fun toString(): String = super.toString()
}

fun<T1, T2> tuple(t1: T1, t2: T2) = ValueTuple2(t1, t2)


data class Layer2_0<out R1, out R2>(
	override val t1: () -> R1,
	override val t2: () -> R2
): Tuple2<() -> R1,
          () -> R2>(t1, t2),
() -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<() -> R1,
	                          () -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke() = tuple(t1(), t2())

	override fun toString(): String = super.toString()
}

fun<R1, R2> tuple(
	f1: () -> R1,
	f2: () -> R2
) = Layer2_0(f1, f2)


data class Layer2_1<out R1, out R2, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2
): Tuple2<(A1) -> R1,
          (A1) -> R2
	  >(t1, t2),
(A1) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1) -> R1,
	                          (A1) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2
) = Layer2_1(f1, f2)


data class Layer2_2<out R1, out R2, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2
): Tuple2<(A1, A2) -> R1,
          (A1, A2) -> R2
	  >(t1, t2),
(A1, A2) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2) -> R1,
	                          (A1, A2) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2
) = Layer2_2(f1, f2)

/*
yGGpjj:s/_\zs\d\+/\=Sub('& + 1')
:s/\d\+\ze>/\=Sub('& . ", in A" . (& + 1)')
j:.,/^)/-1s/\d\+\ze)/\=Sub('& . ", A" . (& + 1)')
j:.,/^\s*>/-1s/\d\+\ze)/\=Sub('& . ", A" . (& + 1)')
jj:.,/^\s*>/-1s/\d\+\ze)/\=Sub('& . ", A" . (& + 1)')
jjj:s/\d\+\ze)/\=Sub('& . ", a" . (& + 1) . ": A" . (& + 1)')
j:.,/^}/s/\d\+\ze)/\=Sub('& . ", a" . (& + 1)')
/^fun
:.,$s/A\zs\d\+\ze[)>]/\=Sub('& . ", A" . (& + 1)')
?data class
kkk
*/

data class Layer2_3<out R1, out R2, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2
): Tuple2<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2
	  >(t1, t2),
(A1, A2, A3) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2
) = Layer2_3(f1, f2)


data class Layer2_4<out R1, out R2, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2
): Tuple2<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2
	  >(t1, t2),
(A1, A2, A3, A4) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2
) = Layer2_4(f1, f2)


data class Layer2_5<out R1, out R2, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2
): Tuple2<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2
	  >(t1, t2),
(A1, A2, A3, A4, A5) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2
) = Layer2_5(f1, f2)


data class Layer2_6<out R1, out R2, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2
): Tuple2<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2
	  >(t1, t2),
(A1, A2, A3, A4, A5, A6) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2
) = Layer2_6(f1, f2)


data class Layer2_7<out R1, out R2, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2
): Tuple2<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2
	  >(t1, t2),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2
) = Layer2_7(f1, f2)


data class Layer2_8<out R1, out R2, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2
): Tuple2<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2
	  >(t1, t2),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2
) = Layer2_8(f1, f2)


data class Layer2_9<out R1, out R2, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2
): Tuple2<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2
	  >(t1, t2),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple2<R1, R2> {
	constructor(tuple: Tuple2<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2
				  >): this(tuple.t1, tuple.t2)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2
) = Layer2_9(f1, f2)



abstract class Tuple3<out T1, out T2, out T3>(
	t1: T1,
	t2: T2,
	open val t3: T3
): Tuple2<T1, T2>(t1, t2) {
	override val size = 3

	inline infix fun<R> feed(f: Callable3<R, in T1, in T2, in T3>): R = f(t1, t2, t3)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)})"
}


data class ValueTuple3<out T1, out T2, out T3>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3
): Tuple3<T1, T2, T3>(t1, t2, t3) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3> tuple(t1: T1, t2: T2, t3: T3) = ValueTuple3(t1, t2, t3)


data class Layer3_0<out R1, out R2, out R3>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3
): Tuple3<() -> R1,
          () -> R2,
          () -> R3
	  >(t1, t2, t3),
() -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<() -> R1,
	                          () -> R2,
	                          () -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke() = tuple(t1(), t2(), t3())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3
) = Layer3_0(f1, f2, f3)


data class Layer3_1<out R1, out R2, out R3, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3
): Tuple3<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3
	  >(t1, t2, t3),
(A1) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3
) = Layer3_1(f1, f2, f3)


data class Layer3_2<out R1, out R2, out R3, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3
): Tuple3<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3
	  >(t1, t2, t3),
(A1, A2) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3
) = Layer3_2(f1, f2, f3)


data class Layer3_3<out R1, out R2, out R3, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3
): Tuple3<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3
	  >(t1, t2, t3),
(A1, A2, A3) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3
) = Layer3_3(f1, f2, f3)


data class Layer3_4<out R1, out R2, out R3, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3
): Tuple3<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3
) = Layer3_4(f1, f2, f3)


data class Layer3_5<out R1, out R2, out R3, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3
): Tuple3<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4, A5) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3
) = Layer3_5(f1, f2, f3)


data class Layer3_6<out R1, out R2, out R3, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3
): Tuple3<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4, A5, A6) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3
) = Layer3_6(f1, f2, f3)


data class Layer3_7<out R1, out R2, out R3, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3
): Tuple3<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3
) = Layer3_7(f1, f2, f3)


data class Layer3_8<out R1, out R2, out R3, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3
): Tuple3<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3
) = Layer3_8(f1, f2, f3)


data class Layer3_9<out R1, out R2, out R3, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3
): Tuple3<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3
	  >(t1, t2, t3),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple3<R1, R2, R3> {
	constructor(tuple: Tuple3<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3
				  >): this(tuple.t1, tuple.t2, tuple.t3)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3
) = Layer3_9(f1, f2, f3)



abstract class Tuple4<out T1, out T2, out T3, out T4>(
	t1: T1,
	t2: T2,
	t3: T3,
	open val t4: T4
): Tuple3<T1, T2, T3>(t1, t2, t3) {
	override val size = 4

	inline infix fun<R> feed(f: Callable4<R, in T1, in T2, in T3, in T4>): R = f(t1, t2, t3, t4)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)})"
}


data class ValueTuple4<out T1, out T2, out T3, out T4>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4
): Tuple4<T1, T2, T3, T4>(t1, t2, t3, t4) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4> tuple(t1: T1, t2: T2, t3: T3, t4: T4) = ValueTuple4(t1, t2, t3, t4)


data class Layer4_0<out R1, out R2, out R3, out R4>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4
): Tuple4<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4
	  >(t1, t2, t3, t4),
() -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4
) = Layer4_0(f1, f2, f3, f4)


data class Layer4_1<out R1, out R2, out R3, out R4, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4
): Tuple4<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4
	  >(t1, t2, t3, t4),
(A1) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4
) = Layer4_1(f1, f2, f3, f4)


data class Layer4_2<out R1, out R2, out R3, out R4, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4
): Tuple4<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4
	  >(t1, t2, t3, t4),
(A1, A2) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4
) = Layer4_2(f1, f2, f3, f4)


data class Layer4_3<out R1, out R2, out R3, out R4, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4
): Tuple4<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4
) = Layer4_3(f1, f2, f3, f4)


data class Layer4_4<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4
): Tuple4<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4
) = Layer4_4(f1, f2, f3, f4)


data class Layer4_5<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4
): Tuple4<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4, A5) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4
) = Layer4_5(f1, f2, f3, f4)


data class Layer4_6<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4
): Tuple4<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4, A5, A6) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4
) = Layer4_6(f1, f2, f3, f4)


data class Layer4_7<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4
): Tuple4<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4
) = Layer4_7(f1, f2, f3, f4)


data class Layer4_8<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4
): Tuple4<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4
) = Layer4_8(f1, f2, f3, f4)


data class Layer4_9<out R1, out R2, out R3, out R4, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4
): Tuple4<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4
	  >(t1, t2, t3, t4),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple4<R1, R2, R3, R4> {
	constructor(tuple: Tuple4<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4
) = Layer4_9(f1, f2, f3, f4)



abstract class Tuple5<out T1, out T2, out T3, out T4, out T5>(
	t1: T1,
	t2: T2,
	t3: T3,
	t4: T4,
	open val t5: T5
): Tuple4<T1, T2, T3, T4>(t1, t2, t3, t4) {
	override val size = 5

	inline infix fun<R> feed(f: Callable5<R, in T1, in T2, in T3, in T4, in T5>): R = f(t1, t2, t3, t4, t5)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)}, ${toString(t5)})"
}


data class ValueTuple5<out T1, out T2, out T3, out T4, out T5>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4,
	override val t5: T5
): Tuple5<T1, T2, T3, T4, T5>(t1, t2, t3, t4, t5) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4, T5> tuple(t1: T1, t2: T2, t3: T3, t4: T4, t5: T5) = ValueTuple5(t1, t2, t3, t4, t5)


data class Layer5_0<out R1, out R2, out R3, out R4, out R5>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4,
	override val t5: () -> R5
): Tuple5<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4,
          () -> R5
	  >(t1, t2, t3, t4, t5),
() -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4,
	                          () -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4(), t5())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4,
	f5: () -> R5
) = Layer5_0(f1, f2, f3, f4, f5)


data class Layer5_1<out R1, out R2, out R3, out R4, out R5, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4,
	override val t5: (A1) -> R5
): Tuple5<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4,
          (A1) -> R5
	  >(t1, t2, t3, t4, t5),
(A1) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4,
	                          (A1) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1),
		t5(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4,
	f5: (A1) -> R5
) = Layer5_1(f1, f2, f3, f4, f5)


data class Layer5_2<out R1, out R2, out R3, out R4, out R5, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4,
	override val t5: (A1, A2) -> R5
): Tuple5<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4,
          (A1, A2) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4,
	                          (A1, A2) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2),
		t5(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4,
	f5: (A1, A2) -> R5
) = Layer5_2(f1, f2, f3, f4, f5)


data class Layer5_3<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4,
	override val t5: (A1, A2, A3) -> R5
): Tuple5<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4,
          (A1, A2, A3) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4,
	                          (A1, A2, A3) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3),
		t5(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4,
	f5: (A1, A2, A3) -> R5
) = Layer5_3(f1, f2, f3, f4, f5)


data class Layer5_4<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4,
	override val t5: (A1, A2, A3, A4) -> R5
): Tuple5<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4,
          (A1, A2, A3, A4) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4,
	                          (A1, A2, A3, A4) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4),
		t5(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4,
	f5: (A1, A2, A3, A4) -> R5
) = Layer5_4(f1, f2, f3, f4, f5)


data class Layer5_5<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4,
	override val t5: (A1, A2, A3, A4, A5) -> R5
): Tuple5<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4,
          (A1, A2, A3, A4, A5) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4, A5) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4,
	                          (A1, A2, A3, A4, A5) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5),
		t5(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4,
	f5: (A1, A2, A3, A4, A5) -> R5
) = Layer5_5(f1, f2, f3, f4, f5)


data class Layer5_6<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6) -> R5
): Tuple5<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4,
          (A1, A2, A3, A4, A5, A6) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4, A5, A6) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4,
	                          (A1, A2, A3, A4, A5, A6) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6),
		t5(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4,
	f5: (A1, A2, A3, A4, A5, A6) -> R5
) = Layer5_6(f1, f2, f3, f4, f5)


data class Layer5_7<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7) -> R5
): Tuple5<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4,
          (A1, A2, A3, A4, A5, A6, A7) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7),
		t5(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7) -> R5
) = Layer5_7(f1, f2, f3, f4, f5)


data class Layer5_8<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5
): Tuple5<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8),
		t5(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5
) = Layer5_8(f1, f2, f3, f4, f5)


data class Layer5_9<out R1, out R2, out R3, out R4, out R5, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5
): Tuple5<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5
	  >(t1, t2, t3, t4, t5),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple5<R1, R2, R3, R4, R5> {
	constructor(tuple: Tuple5<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t5(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5
) = Layer5_9(f1, f2, f3, f4, f5)



abstract class Tuple6<out T1, out T2, out T3, out T4, out T5, out T6>(
	t1: T1,
	t2: T2,
	t3: T3,
	t4: T4,
	t5: T5,
	open val t6: T6
): Tuple5<T1, T2, T3, T4, T5>(t1, t2, t3, t4, t5) {
	override val size = 6

	inline infix fun<R> feed(f: Callable6<R, in T1, in T2, in T3, in T4, in T5, in T6>): R = f(t1, t2, t3, t4, t5, t6)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)}, ${toString(t5)}, ${toString(t6)})"
}


data class ValueTuple6<out T1, out T2, out T3, out T4, out T5, out T6>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4,
	override val t5: T5,
	override val t6: T6
): Tuple6<T1, T2, T3, T4, T5, T6>(t1, t2, t3, t4, t5, t6) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4, T5, T6> tuple(t1: T1, t2: T2, t3: T3, t4: T4, t5: T5, t6: T6) = ValueTuple6(t1, t2, t3, t4, t5, t6)


data class Layer6_0<out R1, out R2, out R3, out R4, out R5, out R6>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4,
	override val t5: () -> R5,
	override val t6: () -> R6
): Tuple6<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4,
          () -> R5,
          () -> R6
	  >(t1, t2, t3, t4, t5, t6),
() -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4,
	                          () -> R5,
	                          () -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4(), t5(), t6())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4,
	f5: () -> R5,
	f6: () -> R6
) = Layer6_0(f1, f2, f3, f4, f5, f6)


data class Layer6_1<out R1, out R2, out R3, out R4, out R5, out R6, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4,
	override val t5: (A1) -> R5,
	override val t6: (A1) -> R6
): Tuple6<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4,
          (A1) -> R5,
          (A1) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4,
	                          (A1) -> R5,
	                          (A1) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1),
		t5(a1),
		t6(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4,
	f5: (A1) -> R5,
	f6: (A1) -> R6
) = Layer6_1(f1, f2, f3, f4, f5, f6)


data class Layer6_2<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4,
	override val t5: (A1, A2) -> R5,
	override val t6: (A1, A2) -> R6
): Tuple6<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4,
          (A1, A2) -> R5,
          (A1, A2) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4,
	                          (A1, A2) -> R5,
	                          (A1, A2) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2),
		t5(a1, a2),
		t6(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4,
	f5: (A1, A2) -> R5,
	f6: (A1, A2) -> R6
) = Layer6_2(f1, f2, f3, f4, f5, f6)


data class Layer6_3<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4,
	override val t5: (A1, A2, A3) -> R5,
	override val t6: (A1, A2, A3) -> R6
): Tuple6<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4,
          (A1, A2, A3) -> R5,
          (A1, A2, A3) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4,
	                          (A1, A2, A3) -> R5,
	                          (A1, A2, A3) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3),
		t5(a1, a2, a3),
		t6(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4,
	f5: (A1, A2, A3) -> R5,
	f6: (A1, A2, A3) -> R6
) = Layer6_3(f1, f2, f3, f4, f5, f6)


data class Layer6_4<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4,
	override val t5: (A1, A2, A3, A4) -> R5,
	override val t6: (A1, A2, A3, A4) -> R6
): Tuple6<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4,
          (A1, A2, A3, A4) -> R5,
          (A1, A2, A3, A4) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4,
	                          (A1, A2, A3, A4) -> R5,
	                          (A1, A2, A3, A4) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4),
		t5(a1, a2, a3, a4),
		t6(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4,
	f5: (A1, A2, A3, A4) -> R5,
	f6: (A1, A2, A3, A4) -> R6
) = Layer6_4(f1, f2, f3, f4, f5, f6)


data class Layer6_5<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4,
	override val t5: (A1, A2, A3, A4, A5) -> R5,
	override val t6: (A1, A2, A3, A4, A5) -> R6
): Tuple6<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4,
          (A1, A2, A3, A4, A5) -> R5,
          (A1, A2, A3, A4, A5) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4, A5) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4,
	                          (A1, A2, A3, A4, A5) -> R5,
	                          (A1, A2, A3, A4, A5) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5),
		t5(a1, a2, a3, a4, a5),
		t6(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4,
	f5: (A1, A2, A3, A4, A5) -> R5,
	f6: (A1, A2, A3, A4, A5) -> R6
) = Layer6_5(f1, f2, f3, f4, f5, f6)


data class Layer6_6<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6) -> R6
): Tuple6<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4,
          (A1, A2, A3, A4, A5, A6) -> R5,
          (A1, A2, A3, A4, A5, A6) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4, A5, A6) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4,
	                          (A1, A2, A3, A4, A5, A6) -> R5,
	                          (A1, A2, A3, A4, A5, A6) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6),
		t5(a1, a2, a3, a4, a5, a6),
		t6(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4,
	f5: (A1, A2, A3, A4, A5, A6) -> R5,
	f6: (A1, A2, A3, A4, A5, A6) -> R6
) = Layer6_6(f1, f2, f3, f4, f5, f6)


data class Layer6_7<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7) -> R6
): Tuple6<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4,
          (A1, A2, A3, A4, A5, A6, A7) -> R5,
          (A1, A2, A3, A4, A5, A6, A7) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7),
		t5(a1, a2, a3, a4, a5, a6, a7),
		t6(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7) -> R6
) = Layer6_7(f1, f2, f3, f4, f5, f6)


data class Layer6_8<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6
): Tuple6<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8),
		t5(a1, a2, a3, a4, a5, a6, a7, a8),
		t6(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6
) = Layer6_8(f1, f2, f3, f4, f5, f6)


data class Layer6_9<out R1, out R2, out R3, out R4, out R5, out R6, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6
): Tuple6<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6
	  >(t1, t2, t3, t4, t5, t6),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple6<R1, R2, R3, R4, R5, R6> {
	constructor(tuple: Tuple6<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t5(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t6(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6
) = Layer6_9(f1, f2, f3, f4, f5, f6)



abstract class Tuple7<out T1, out T2, out T3, out T4, out T5, out T6, out T7>(
	t1: T1,
	t2: T2,
	t3: T3,
	t4: T4,
	t5: T5,
	t6: T6,
	open val t7: T7
): Tuple6<T1, T2, T3, T4, T5, T6>(t1, t2, t3, t4, t5, t6) {
	override val size = 7

	inline infix fun<R> feed(f: Callable7<R, in T1, in T2, in T3, in T4, in T5, in T6, in T7>): R = f(t1, t2, t3, t4, t5, t6, t7)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)}, ${toString(t5)}, ${toString(t6)}, ${toString(t7)})"
}


data class ValueTuple7<out T1, out T2, out T3, out T4, out T5, out T6, out T7>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4,
	override val t5: T5,
	override val t6: T6,
	override val t7: T7
): Tuple7<T1, T2, T3, T4, T5, T6, T7>(t1, t2, t3, t4, t5, t6, t7) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4, T5, T6, T7> tuple(t1: T1, t2: T2, t3: T3, t4: T4, t5: T5, t6: T6, t7: T7) = ValueTuple7(t1, t2, t3, t4, t5, t6, t7)


data class Layer7_0<out R1, out R2, out R3, out R4, out R5, out R6, out R7>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4,
	override val t5: () -> R5,
	override val t6: () -> R6,
	override val t7: () -> R7
): Tuple7<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4,
          () -> R5,
          () -> R6,
          () -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
() -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4,
	                          () -> R5,
	                          () -> R6,
	                          () -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4(), t5(), t6(), t7())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4,
	f5: () -> R5,
	f6: () -> R6,
	f7: () -> R7
) = Layer7_0(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_1<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4,
	override val t5: (A1) -> R5,
	override val t6: (A1) -> R6,
	override val t7: (A1) -> R7
): Tuple7<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4,
          (A1) -> R5,
          (A1) -> R6,
          (A1) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4,
	                          (A1) -> R5,
	                          (A1) -> R6,
	                          (A1) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1),
		t5(a1),
		t6(a1),
		t7(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4,
	f5: (A1) -> R5,
	f6: (A1) -> R6,
	f7: (A1) -> R7
) = Layer7_1(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_2<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4,
	override val t5: (A1, A2) -> R5,
	override val t6: (A1, A2) -> R6,
	override val t7: (A1, A2) -> R7
): Tuple7<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4,
          (A1, A2) -> R5,
          (A1, A2) -> R6,
          (A1, A2) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4,
	                          (A1, A2) -> R5,
	                          (A1, A2) -> R6,
	                          (A1, A2) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2),
		t5(a1, a2),
		t6(a1, a2),
		t7(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4,
	f5: (A1, A2) -> R5,
	f6: (A1, A2) -> R6,
	f7: (A1, A2) -> R7
) = Layer7_2(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_3<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4,
	override val t5: (A1, A2, A3) -> R5,
	override val t6: (A1, A2, A3) -> R6,
	override val t7: (A1, A2, A3) -> R7
): Tuple7<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4,
          (A1, A2, A3) -> R5,
          (A1, A2, A3) -> R6,
          (A1, A2, A3) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4,
	                          (A1, A2, A3) -> R5,
	                          (A1, A2, A3) -> R6,
	                          (A1, A2, A3) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3),
		t5(a1, a2, a3),
		t6(a1, a2, a3),
		t7(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4,
	f5: (A1, A2, A3) -> R5,
	f6: (A1, A2, A3) -> R6,
	f7: (A1, A2, A3) -> R7
) = Layer7_3(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_4<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4,
	override val t5: (A1, A2, A3, A4) -> R5,
	override val t6: (A1, A2, A3, A4) -> R6,
	override val t7: (A1, A2, A3, A4) -> R7
): Tuple7<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4,
          (A1, A2, A3, A4) -> R5,
          (A1, A2, A3, A4) -> R6,
          (A1, A2, A3, A4) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4,
	                          (A1, A2, A3, A4) -> R5,
	                          (A1, A2, A3, A4) -> R6,
	                          (A1, A2, A3, A4) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4),
		t5(a1, a2, a3, a4),
		t6(a1, a2, a3, a4),
		t7(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4,
	f5: (A1, A2, A3, A4) -> R5,
	f6: (A1, A2, A3, A4) -> R6,
	f7: (A1, A2, A3, A4) -> R7
) = Layer7_4(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_5<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4,
	override val t5: (A1, A2, A3, A4, A5) -> R5,
	override val t6: (A1, A2, A3, A4, A5) -> R6,
	override val t7: (A1, A2, A3, A4, A5) -> R7
): Tuple7<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4,
          (A1, A2, A3, A4, A5) -> R5,
          (A1, A2, A3, A4, A5) -> R6,
          (A1, A2, A3, A4, A5) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4, A5) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4,
	                          (A1, A2, A3, A4, A5) -> R5,
	                          (A1, A2, A3, A4, A5) -> R6,
	                          (A1, A2, A3, A4, A5) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5),
		t5(a1, a2, a3, a4, a5),
		t6(a1, a2, a3, a4, a5),
		t7(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4,
	f5: (A1, A2, A3, A4, A5) -> R5,
	f6: (A1, A2, A3, A4, A5) -> R6,
	f7: (A1, A2, A3, A4, A5) -> R7
) = Layer7_5(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_6<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6) -> R7
): Tuple7<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4,
          (A1, A2, A3, A4, A5, A6) -> R5,
          (A1, A2, A3, A4, A5, A6) -> R6,
          (A1, A2, A3, A4, A5, A6) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4, A5, A6) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4,
	                          (A1, A2, A3, A4, A5, A6) -> R5,
	                          (A1, A2, A3, A4, A5, A6) -> R6,
	                          (A1, A2, A3, A4, A5, A6) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6),
		t5(a1, a2, a3, a4, a5, a6),
		t6(a1, a2, a3, a4, a5, a6),
		t7(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4,
	f5: (A1, A2, A3, A4, A5, A6) -> R5,
	f6: (A1, A2, A3, A4, A5, A6) -> R6,
	f7: (A1, A2, A3, A4, A5, A6) -> R7
) = Layer7_6(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_7<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7) -> R7
): Tuple7<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4,
          (A1, A2, A3, A4, A5, A6, A7) -> R5,
          (A1, A2, A3, A4, A5, A6, A7) -> R6,
          (A1, A2, A3, A4, A5, A6, A7) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7),
		t5(a1, a2, a3, a4, a5, a6, a7),
		t6(a1, a2, a3, a4, a5, a6, a7),
		t7(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7) -> R7
) = Layer7_7(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_8<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7
): Tuple7<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8),
		t5(a1, a2, a3, a4, a5, a6, a7, a8),
		t6(a1, a2, a3, a4, a5, a6, a7, a8),
		t7(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7
) = Layer7_8(f1, f2, f3, f4, f5, f6, f7)


data class Layer7_9<out R1, out R2, out R3, out R4, out R5, out R6, out R7, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7
): Tuple7<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7
	  >(t1, t2, t3, t4, t5, t6, t7),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple7<R1, R2, R3, R4, R5, R6, R7> {
	constructor(tuple: Tuple7<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t5(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t6(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t7(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7
) = Layer7_9(f1, f2, f3, f4, f5, f6, f7)



abstract class Tuple8<out T1, out T2, out T3, out T4, out T5, out T6, out T7, out T8>(
	t1: T1,
	t2: T2,
	t3: T3,
	t4: T4,
	t5: T5,
	t6: T6,
	t7: T7,
	open val t8: T8
): Tuple7<T1, T2, T3, T4, T5, T6, T7>(t1, t2, t3, t4, t5, t6, t7) {
	override val size = 8

	inline infix fun<R> feed(f: Callable8<R, in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8>): R = f(t1, t2, t3, t4, t5, t6, t7, t8)

	override fun toString(): String = "Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)}, ${toString(t5)}, ${toString(t6)}, ${toString(t7)}, ${toString(t8)})"
}


data class ValueTuple8<out T1, out T2, out T3, out T4, out T5, out T6, out T7, out T8>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4,
	override val t5: T5,
	override val t6: T6,
	override val t7: T7,
	override val t8: T8
): Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(t1, t2, t3, t4, t5, t6, t7, t8) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4, T5, T6, T7, T8> tuple(t1: T1, t2: T2, t3: T3, t4: T4, t5: T5, t6: T6, t7: T7, t8: T8) = ValueTuple8(t1, t2, t3, t4, t5, t6, t7, t8)


data class Layer8_0<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4,
	override val t5: () -> R5,
	override val t6: () -> R6,
	override val t7: () -> R7,
	override val t8: () -> R8
): Tuple8<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4,
          () -> R5,
          () -> R6,
          () -> R7,
          () -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
() -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4,
	                          () -> R5,
	                          () -> R6,
	                          () -> R7,
	                          () -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4(), t5(), t6(), t7(), t8())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4,
	f5: () -> R5,
	f6: () -> R6,
	f7: () -> R7,
	f8: () -> R8
) = Layer8_0(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_1<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4,
	override val t5: (A1) -> R5,
	override val t6: (A1) -> R6,
	override val t7: (A1) -> R7,
	override val t8: (A1) -> R8
): Tuple8<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4,
          (A1) -> R5,
          (A1) -> R6,
          (A1) -> R7,
          (A1) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4,
	                          (A1) -> R5,
	                          (A1) -> R6,
	                          (A1) -> R7,
	                          (A1) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1),
		t5(a1),
		t6(a1),
		t7(a1),
		t8(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4,
	f5: (A1) -> R5,
	f6: (A1) -> R6,
	f7: (A1) -> R7,
	f8: (A1) -> R8
) = Layer8_1(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_2<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4,
	override val t5: (A1, A2) -> R5,
	override val t6: (A1, A2) -> R6,
	override val t7: (A1, A2) -> R7,
	override val t8: (A1, A2) -> R8
): Tuple8<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4,
          (A1, A2) -> R5,
          (A1, A2) -> R6,
          (A1, A2) -> R7,
          (A1, A2) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4,
	                          (A1, A2) -> R5,
	                          (A1, A2) -> R6,
	                          (A1, A2) -> R7,
	                          (A1, A2) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2),
		t5(a1, a2),
		t6(a1, a2),
		t7(a1, a2),
		t8(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4,
	f5: (A1, A2) -> R5,
	f6: (A1, A2) -> R6,
	f7: (A1, A2) -> R7,
	f8: (A1, A2) -> R8
) = Layer8_2(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_3<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4,
	override val t5: (A1, A2, A3) -> R5,
	override val t6: (A1, A2, A3) -> R6,
	override val t7: (A1, A2, A3) -> R7,
	override val t8: (A1, A2, A3) -> R8
): Tuple8<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4,
          (A1, A2, A3) -> R5,
          (A1, A2, A3) -> R6,
          (A1, A2, A3) -> R7,
          (A1, A2, A3) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4,
	                          (A1, A2, A3) -> R5,
	                          (A1, A2, A3) -> R6,
	                          (A1, A2, A3) -> R7,
	                          (A1, A2, A3) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3),
		t5(a1, a2, a3),
		t6(a1, a2, a3),
		t7(a1, a2, a3),
		t8(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4,
	f5: (A1, A2, A3) -> R5,
	f6: (A1, A2, A3) -> R6,
	f7: (A1, A2, A3) -> R7,
	f8: (A1, A2, A3) -> R8
) = Layer8_3(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_4<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4,
	override val t5: (A1, A2, A3, A4) -> R5,
	override val t6: (A1, A2, A3, A4) -> R6,
	override val t7: (A1, A2, A3, A4) -> R7,
	override val t8: (A1, A2, A3, A4) -> R8
): Tuple8<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4,
          (A1, A2, A3, A4) -> R5,
          (A1, A2, A3, A4) -> R6,
          (A1, A2, A3, A4) -> R7,
          (A1, A2, A3, A4) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4,
	                          (A1, A2, A3, A4) -> R5,
	                          (A1, A2, A3, A4) -> R6,
	                          (A1, A2, A3, A4) -> R7,
	                          (A1, A2, A3, A4) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4),
		t5(a1, a2, a3, a4),
		t6(a1, a2, a3, a4),
		t7(a1, a2, a3, a4),
		t8(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4,
	f5: (A1, A2, A3, A4) -> R5,
	f6: (A1, A2, A3, A4) -> R6,
	f7: (A1, A2, A3, A4) -> R7,
	f8: (A1, A2, A3, A4) -> R8
) = Layer8_4(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_5<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4,
	override val t5: (A1, A2, A3, A4, A5) -> R5,
	override val t6: (A1, A2, A3, A4, A5) -> R6,
	override val t7: (A1, A2, A3, A4, A5) -> R7,
	override val t8: (A1, A2, A3, A4, A5) -> R8
): Tuple8<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4,
          (A1, A2, A3, A4, A5) -> R5,
          (A1, A2, A3, A4, A5) -> R6,
          (A1, A2, A3, A4, A5) -> R7,
          (A1, A2, A3, A4, A5) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4, A5) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4,
	                          (A1, A2, A3, A4, A5) -> R5,
	                          (A1, A2, A3, A4, A5) -> R6,
	                          (A1, A2, A3, A4, A5) -> R7,
	                          (A1, A2, A3, A4, A5) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5),
		t5(a1, a2, a3, a4, a5),
		t6(a1, a2, a3, a4, a5),
		t7(a1, a2, a3, a4, a5),
		t8(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4,
	f5: (A1, A2, A3, A4, A5) -> R5,
	f6: (A1, A2, A3, A4, A5) -> R6,
	f7: (A1, A2, A3, A4, A5) -> R7,
	f8: (A1, A2, A3, A4, A5) -> R8
) = Layer8_5(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_6<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6) -> R8
): Tuple8<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4,
          (A1, A2, A3, A4, A5, A6) -> R5,
          (A1, A2, A3, A4, A5, A6) -> R6,
          (A1, A2, A3, A4, A5, A6) -> R7,
          (A1, A2, A3, A4, A5, A6) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4, A5, A6) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4,
	                          (A1, A2, A3, A4, A5, A6) -> R5,
	                          (A1, A2, A3, A4, A5, A6) -> R6,
	                          (A1, A2, A3, A4, A5, A6) -> R7,
	                          (A1, A2, A3, A4, A5, A6) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6),
		t5(a1, a2, a3, a4, a5, a6),
		t6(a1, a2, a3, a4, a5, a6),
		t7(a1, a2, a3, a4, a5, a6),
		t8(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4,
	f5: (A1, A2, A3, A4, A5, A6) -> R5,
	f6: (A1, A2, A3, A4, A5, A6) -> R6,
	f7: (A1, A2, A3, A4, A5, A6) -> R7,
	f8: (A1, A2, A3, A4, A5, A6) -> R8
) = Layer8_6(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_7<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7) -> R8
): Tuple8<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4,
          (A1, A2, A3, A4, A5, A6, A7) -> R5,
          (A1, A2, A3, A4, A5, A6, A7) -> R6,
          (A1, A2, A3, A4, A5, A6, A7) -> R7,
          (A1, A2, A3, A4, A5, A6, A7) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7),
		t5(a1, a2, a3, a4, a5, a6, a7),
		t6(a1, a2, a3, a4, a5, a6, a7),
		t7(a1, a2, a3, a4, a5, a6, a7),
		t8(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7) -> R8
) = Layer8_7(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_8<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7, A8) -> R8
): Tuple8<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8),
		t5(a1, a2, a3, a4, a5, a6, a7, a8),
		t6(a1, a2, a3, a4, a5, a6, a7, a8),
		t7(a1, a2, a3, a4, a5, a6, a7, a8),
		t8(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7, A8) -> R8
) = Layer8_8(f1, f2, f3, f4, f5, f6, f7, f8)


data class Layer8_9<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8
): Tuple8<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8
	  >(t1, t2, t3, t4, t5, t6, t7, t8),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple8<R1, R2, R3, R4, R5, R6, R7, R8> {
	constructor(tuple: Tuple8<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t5(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t6(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t7(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t8(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8
) = Layer8_9(f1, f2, f3, f4, f5, f6, f7, f8)



abstract class Tuple9<out T1, out T2, out T3, out T4, out T5, out T6, out T7, out T8, out T9>(
	t1: T1,
	t2: T2,
	t3: T3,
	t4: T4,
	t5: T5,
	t6: T6,
	t7: T7,
	t8: T8,
	open val t9: T9
): Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(t1, t2, t3, t4, t5, t6, t7, t8) {
	override val size = 9

	inline infix fun<R> feed(f: Callable9<R, in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9>): R = f(t1, t2, t3, t4, t5, t6, t7, t8, t9)

	override fun toString(): String =
		"Tuple(${toString(t1)}, ${toString(t2)}, ${toString(t3)}, ${toString(t4)}, ${toString(t5)}, ${toString(t6)}, ${toString(t7)}, ${toString(t8)}, ${toString(t9)})"
}


data class ValueTuple9<out T1, out T2, out T3, out T4, out T5, out T6, out T7, out T8, out T9>(
	override val t1: T1,
	override val t2: T2,
	override val t3: T3,
	override val t4: T4,
	override val t5: T5,
	override val t6: T6,
	override val t7: T7,
	override val t8: T8,
	override val t9: T9
): Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>(t1, t2, t3, t4, t5, t6, t7, t8, t9) {
	override fun toString(): String = super.toString()
}

fun<T1, T2, T3, T4, T5, T6, T7, T8, T9> tuple(t1: T1, t2: T2, t3: T3, t4: T4, t5: T5, t6: T6, t7: T7, t8: T8, t9: T9) = ValueTuple9(t1, t2, t3, t4, t5, t6, t7, t8, t9)


data class Layer9_0<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9>(
	override val t1: () -> R1,
	override val t2: () -> R2,
	override val t3: () -> R3,
	override val t4: () -> R4,
	override val t5: () -> R5,
	override val t6: () -> R6,
	override val t7: () -> R7,
	override val t8: () -> R8,
	override val t9: () -> R9
): Tuple9<() -> R1,
          () -> R2,
          () -> R3,
          () -> R4,
          () -> R5,
          () -> R6,
          () -> R7,
          () -> R8,
          () -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
() -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<() -> R1,
	                          () -> R2,
	                          () -> R3,
	                          () -> R4,
	                          () -> R5,
	                          () -> R6,
	                          () -> R7,
	                          () -> R8,
	                          () -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke() = tuple(t1(), t2(), t3(), t4(), t5(), t6(), t7(), t8(), t9())

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9> tuple(
	f1: () -> R1,
	f2: () -> R2,
	f3: () -> R3,
	f4: () -> R4,
	f5: () -> R5,
	f6: () -> R6,
	f7: () -> R7,
	f8: () -> R8,
	f9: () -> R9
) = Layer9_0(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_1<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1>(
	override val t1: (A1) -> R1,
	override val t2: (A1) -> R2,
	override val t3: (A1) -> R3,
	override val t4: (A1) -> R4,
	override val t5: (A1) -> R5,
	override val t6: (A1) -> R6,
	override val t7: (A1) -> R7,
	override val t8: (A1) -> R8,
	override val t9: (A1) -> R9
): Tuple9<(A1) -> R1,
          (A1) -> R2,
          (A1) -> R3,
          (A1) -> R4,
          (A1) -> R5,
          (A1) -> R6,
          (A1) -> R7,
          (A1) -> R8,
          (A1) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1) -> R1,
	                          (A1) -> R2,
	                          (A1) -> R3,
	                          (A1) -> R4,
	                          (A1) -> R5,
	                          (A1) -> R6,
	                          (A1) -> R7,
	                          (A1) -> R8,
	                          (A1) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1) = tuple(
		t1(a1),
		t2(a1),
		t3(a1),
		t4(a1),
		t5(a1),
		t6(a1),
		t7(a1),
		t8(a1),
		t9(a1)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1> tuple(
	f1: (A1) -> R1,
	f2: (A1) -> R2,
	f3: (A1) -> R3,
	f4: (A1) -> R4,
	f5: (A1) -> R5,
	f6: (A1) -> R6,
	f7: (A1) -> R7,
	f8: (A1) -> R8,
	f9: (A1) -> R9
) = Layer9_1(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_2<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2>(
	override val t1: (A1, A2) -> R1,
	override val t2: (A1, A2) -> R2,
	override val t3: (A1, A2) -> R3,
	override val t4: (A1, A2) -> R4,
	override val t5: (A1, A2) -> R5,
	override val t6: (A1, A2) -> R6,
	override val t7: (A1, A2) -> R7,
	override val t8: (A1, A2) -> R8,
	override val t9: (A1, A2) -> R9
): Tuple9<(A1, A2) -> R1,
          (A1, A2) -> R2,
          (A1, A2) -> R3,
          (A1, A2) -> R4,
          (A1, A2) -> R5,
          (A1, A2) -> R6,
          (A1, A2) -> R7,
          (A1, A2) -> R8,
          (A1, A2) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2) -> R1,
	                          (A1, A2) -> R2,
	                          (A1, A2) -> R3,
	                          (A1, A2) -> R4,
	                          (A1, A2) -> R5,
	                          (A1, A2) -> R6,
	                          (A1, A2) -> R7,
	                          (A1, A2) -> R8,
	                          (A1, A2) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2) = tuple(
		t1(a1, a2),
		t2(a1, a2),
		t3(a1, a2),
		t4(a1, a2),
		t5(a1, a2),
		t6(a1, a2),
		t7(a1, a2),
		t8(a1, a2),
		t9(a1, a2)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2> tuple(
	f1: (A1, A2) -> R1,
	f2: (A1, A2) -> R2,
	f3: (A1, A2) -> R3,
	f4: (A1, A2) -> R4,
	f5: (A1, A2) -> R5,
	f6: (A1, A2) -> R6,
	f7: (A1, A2) -> R7,
	f8: (A1, A2) -> R8,
	f9: (A1, A2) -> R9
) = Layer9_2(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_3<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3>(
	override val t1: (A1, A2, A3) -> R1,
	override val t2: (A1, A2, A3) -> R2,
	override val t3: (A1, A2, A3) -> R3,
	override val t4: (A1, A2, A3) -> R4,
	override val t5: (A1, A2, A3) -> R5,
	override val t6: (A1, A2, A3) -> R6,
	override val t7: (A1, A2, A3) -> R7,
	override val t8: (A1, A2, A3) -> R8,
	override val t9: (A1, A2, A3) -> R9
): Tuple9<(A1, A2, A3) -> R1,
          (A1, A2, A3) -> R2,
          (A1, A2, A3) -> R3,
          (A1, A2, A3) -> R4,
          (A1, A2, A3) -> R5,
          (A1, A2, A3) -> R6,
          (A1, A2, A3) -> R7,
          (A1, A2, A3) -> R8,
          (A1, A2, A3) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3) -> R1,
	                          (A1, A2, A3) -> R2,
	                          (A1, A2, A3) -> R3,
	                          (A1, A2, A3) -> R4,
	                          (A1, A2, A3) -> R5,
	                          (A1, A2, A3) -> R6,
	                          (A1, A2, A3) -> R7,
	                          (A1, A2, A3) -> R8,
	                          (A1, A2, A3) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3) = tuple(
		t1(a1, a2, a3),
		t2(a1, a2, a3),
		t3(a1, a2, a3),
		t4(a1, a2, a3),
		t5(a1, a2, a3),
		t6(a1, a2, a3),
		t7(a1, a2, a3),
		t8(a1, a2, a3),
		t9(a1, a2, a3)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3> tuple(
	f1: (A1, A2, A3) -> R1,
	f2: (A1, A2, A3) -> R2,
	f3: (A1, A2, A3) -> R3,
	f4: (A1, A2, A3) -> R4,
	f5: (A1, A2, A3) -> R5,
	f6: (A1, A2, A3) -> R6,
	f7: (A1, A2, A3) -> R7,
	f8: (A1, A2, A3) -> R8,
	f9: (A1, A2, A3) -> R9
) = Layer9_3(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_4<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4>(
	override val t1: (A1, A2, A3, A4) -> R1,
	override val t2: (A1, A2, A3, A4) -> R2,
	override val t3: (A1, A2, A3, A4) -> R3,
	override val t4: (A1, A2, A3, A4) -> R4,
	override val t5: (A1, A2, A3, A4) -> R5,
	override val t6: (A1, A2, A3, A4) -> R6,
	override val t7: (A1, A2, A3, A4) -> R7,
	override val t8: (A1, A2, A3, A4) -> R8,
	override val t9: (A1, A2, A3, A4) -> R9
): Tuple9<(A1, A2, A3, A4) -> R1,
          (A1, A2, A3, A4) -> R2,
          (A1, A2, A3, A4) -> R3,
          (A1, A2, A3, A4) -> R4,
          (A1, A2, A3, A4) -> R5,
          (A1, A2, A3, A4) -> R6,
          (A1, A2, A3, A4) -> R7,
          (A1, A2, A3, A4) -> R8,
          (A1, A2, A3, A4) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4) -> R1,
	                          (A1, A2, A3, A4) -> R2,
	                          (A1, A2, A3, A4) -> R3,
	                          (A1, A2, A3, A4) -> R4,
	                          (A1, A2, A3, A4) -> R5,
	                          (A1, A2, A3, A4) -> R6,
	                          (A1, A2, A3, A4) -> R7,
	                          (A1, A2, A3, A4) -> R8,
	                          (A1, A2, A3, A4) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4) = tuple(
		t1(a1, a2, a3, a4),
		t2(a1, a2, a3, a4),
		t3(a1, a2, a3, a4),
		t4(a1, a2, a3, a4),
		t5(a1, a2, a3, a4),
		t6(a1, a2, a3, a4),
		t7(a1, a2, a3, a4),
		t8(a1, a2, a3, a4),
		t9(a1, a2, a3, a4)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4> tuple(
	f1: (A1, A2, A3, A4) -> R1,
	f2: (A1, A2, A3, A4) -> R2,
	f3: (A1, A2, A3, A4) -> R3,
	f4: (A1, A2, A3, A4) -> R4,
	f5: (A1, A2, A3, A4) -> R5,
	f6: (A1, A2, A3, A4) -> R6,
	f7: (A1, A2, A3, A4) -> R7,
	f8: (A1, A2, A3, A4) -> R8,
	f9: (A1, A2, A3, A4) -> R9
) = Layer9_4(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_5<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4, in A5>(
	override val t1: (A1, A2, A3, A4, A5) -> R1,
	override val t2: (A1, A2, A3, A4, A5) -> R2,
	override val t3: (A1, A2, A3, A4, A5) -> R3,
	override val t4: (A1, A2, A3, A4, A5) -> R4,
	override val t5: (A1, A2, A3, A4, A5) -> R5,
	override val t6: (A1, A2, A3, A4, A5) -> R6,
	override val t7: (A1, A2, A3, A4, A5) -> R7,
	override val t8: (A1, A2, A3, A4, A5) -> R8,
	override val t9: (A1, A2, A3, A4, A5) -> R9
): Tuple9<(A1, A2, A3, A4, A5) -> R1,
          (A1, A2, A3, A4, A5) -> R2,
          (A1, A2, A3, A4, A5) -> R3,
          (A1, A2, A3, A4, A5) -> R4,
          (A1, A2, A3, A4, A5) -> R5,
          (A1, A2, A3, A4, A5) -> R6,
          (A1, A2, A3, A4, A5) -> R7,
          (A1, A2, A3, A4, A5) -> R8,
          (A1, A2, A3, A4, A5) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4, A5) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4, A5) -> R1,
	                          (A1, A2, A3, A4, A5) -> R2,
	                          (A1, A2, A3, A4, A5) -> R3,
	                          (A1, A2, A3, A4, A5) -> R4,
	                          (A1, A2, A3, A4, A5) -> R5,
	                          (A1, A2, A3, A4, A5) -> R6,
	                          (A1, A2, A3, A4, A5) -> R7,
	                          (A1, A2, A3, A4, A5) -> R8,
	                          (A1, A2, A3, A4, A5) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) = tuple(
		t1(a1, a2, a3, a4, a5),
		t2(a1, a2, a3, a4, a5),
		t3(a1, a2, a3, a4, a5),
		t4(a1, a2, a3, a4, a5),
		t5(a1, a2, a3, a4, a5),
		t6(a1, a2, a3, a4, a5),
		t7(a1, a2, a3, a4, a5),
		t8(a1, a2, a3, a4, a5),
		t9(a1, a2, a3, a4, a5)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4, A5> tuple(
	f1: (A1, A2, A3, A4, A5) -> R1,
	f2: (A1, A2, A3, A4, A5) -> R2,
	f3: (A1, A2, A3, A4, A5) -> R3,
	f4: (A1, A2, A3, A4, A5) -> R4,
	f5: (A1, A2, A3, A4, A5) -> R5,
	f6: (A1, A2, A3, A4, A5) -> R6,
	f7: (A1, A2, A3, A4, A5) -> R7,
	f8: (A1, A2, A3, A4, A5) -> R8,
	f9: (A1, A2, A3, A4, A5) -> R9
) = Layer9_5(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_6<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4, in A5, in A6>(
	override val t1: (A1, A2, A3, A4, A5, A6) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6) -> R8,
	override val t9: (A1, A2, A3, A4, A5, A6) -> R9
): Tuple9<(A1, A2, A3, A4, A5, A6) -> R1,
          (A1, A2, A3, A4, A5, A6) -> R2,
          (A1, A2, A3, A4, A5, A6) -> R3,
          (A1, A2, A3, A4, A5, A6) -> R4,
          (A1, A2, A3, A4, A5, A6) -> R5,
          (A1, A2, A3, A4, A5, A6) -> R6,
          (A1, A2, A3, A4, A5, A6) -> R7,
          (A1, A2, A3, A4, A5, A6) -> R8,
          (A1, A2, A3, A4, A5, A6) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4, A5, A6) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4, A5, A6) -> R1,
	                          (A1, A2, A3, A4, A5, A6) -> R2,
	                          (A1, A2, A3, A4, A5, A6) -> R3,
	                          (A1, A2, A3, A4, A5, A6) -> R4,
	                          (A1, A2, A3, A4, A5, A6) -> R5,
	                          (A1, A2, A3, A4, A5, A6) -> R6,
	                          (A1, A2, A3, A4, A5, A6) -> R7,
	                          (A1, A2, A3, A4, A5, A6) -> R8,
	                          (A1, A2, A3, A4, A5, A6) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) = tuple(
		t1(a1, a2, a3, a4, a5, a6),
		t2(a1, a2, a3, a4, a5, a6),
		t3(a1, a2, a3, a4, a5, a6),
		t4(a1, a2, a3, a4, a5, a6),
		t5(a1, a2, a3, a4, a5, a6),
		t6(a1, a2, a3, a4, a5, a6),
		t7(a1, a2, a3, a4, a5, a6),
		t8(a1, a2, a3, a4, a5, a6),
		t9(a1, a2, a3, a4, a5, a6)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4, A5, A6> tuple(
	f1: (A1, A2, A3, A4, A5, A6) -> R1,
	f2: (A1, A2, A3, A4, A5, A6) -> R2,
	f3: (A1, A2, A3, A4, A5, A6) -> R3,
	f4: (A1, A2, A3, A4, A5, A6) -> R4,
	f5: (A1, A2, A3, A4, A5, A6) -> R5,
	f6: (A1, A2, A3, A4, A5, A6) -> R6,
	f7: (A1, A2, A3, A4, A5, A6) -> R7,
	f8: (A1, A2, A3, A4, A5, A6) -> R8,
	f9: (A1, A2, A3, A4, A5, A6) -> R9
) = Layer9_6(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_7<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4, in A5, in A6, in A7>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7) -> R8,
	override val t9: (A1, A2, A3, A4, A5, A6, A7) -> R9
): Tuple9<(A1, A2, A3, A4, A5, A6, A7) -> R1,
          (A1, A2, A3, A4, A5, A6, A7) -> R2,
          (A1, A2, A3, A4, A5, A6, A7) -> R3,
          (A1, A2, A3, A4, A5, A6, A7) -> R4,
          (A1, A2, A3, A4, A5, A6, A7) -> R5,
          (A1, A2, A3, A4, A5, A6, A7) -> R6,
          (A1, A2, A3, A4, A5, A6, A7) -> R7,
          (A1, A2, A3, A4, A5, A6, A7) -> R8,
          (A1, A2, A3, A4, A5, A6, A7) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4, A5, A6, A7) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4, A5, A6, A7) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R8,
	                          (A1, A2, A3, A4, A5, A6, A7) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7),
		t2(a1, a2, a3, a4, a5, a6, a7),
		t3(a1, a2, a3, a4, a5, a6, a7),
		t4(a1, a2, a3, a4, a5, a6, a7),
		t5(a1, a2, a3, a4, a5, a6, a7),
		t6(a1, a2, a3, a4, a5, a6, a7),
		t7(a1, a2, a3, a4, a5, a6, a7),
		t8(a1, a2, a3, a4, a5, a6, a7),
		t9(a1, a2, a3, a4, a5, a6, a7)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4, A5, A6, A7> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7) -> R8,
	f9: (A1, A2, A3, A4, A5, A6, A7) -> R9
) = Layer9_7(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_8<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7, A8) -> R8,
	override val t9: (A1, A2, A3, A4, A5, A6, A7, A8) -> R9
): Tuple9<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R8,
          (A1, A2, A3, A4, A5, A6, A7, A8) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4, A5, A6, A7, A8) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R8,
	                          (A1, A2, A3, A4, A5, A6, A7, A8) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8),
		t2(a1, a2, a3, a4, a5, a6, a7, a8),
		t3(a1, a2, a3, a4, a5, a6, a7, a8),
		t4(a1, a2, a3, a4, a5, a6, a7, a8),
		t5(a1, a2, a3, a4, a5, a6, a7, a8),
		t6(a1, a2, a3, a4, a5, a6, a7, a8),
		t7(a1, a2, a3, a4, a5, a6, a7, a8),
		t8(a1, a2, a3, a4, a5, a6, a7, a8),
		t9(a1, a2, a3, a4, a5, a6, a7, a8)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4, A5, A6, A7, A8> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7, A8) -> R8,
	f9: (A1, A2, A3, A4, A5, A6, A7, A8) -> R9
) = Layer9_8(f1, f2, f3, f4, f5, f6, f7, f8, f9)


data class Layer9_9<out R1, out R2, out R3, out R4, out R5, out R6, out R7, out R8, out R9, in A1, in A2, in A3, in A4, in A5, in A6, in A7, in A8, in A9>(
	override val t1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	override val t2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	override val t3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	override val t4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	override val t5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	override val t6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	override val t7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	override val t8: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8,
	override val t9: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R9
): Tuple9<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8,
          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R9
	  >(t1, t2, t3, t4, t5, t6, t7, t8, t9),
(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> Tuple9<R1, R2, R3, R4, R5, R6, R7, R8, R9> {
	constructor(tuple: Tuple9<(A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8,
	                          (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R9
				  >): this(tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8, tuple.t9)

	override operator fun invoke(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) = tuple(
		t1(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t2(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t3(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t4(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t5(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t6(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t7(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t8(a1, a2, a3, a4, a5, a6, a7, a8, a9),
		t9(a1, a2, a3, a4, a5, a6, a7, a8, a9)
	)

	override fun toString(): String = super.toString()
}

fun<R1, R2, R3, R4, R5, R6, R7, R8, R9, A1, A2, A3, A4, A5, A6, A7, A8, A9> tuple(
	f1: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R1,
	f2: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R2,
	f3: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R3,
	f4: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R4,
	f5: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R5,
	f6: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R6,
	f7: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R7,
	f8: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R8,
	f9: (A1, A2, A3, A4, A5, A6, A7, A8, A9) -> R9
) = Layer9_9(f1, f2, f3, f4, f5, f6, f7, f8, f9)
